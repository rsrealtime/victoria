﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Project  {
    public Dictionary<string,string> ids = new Dictionary<string, string>();
    public string name;
    public IDataService provider;
}
